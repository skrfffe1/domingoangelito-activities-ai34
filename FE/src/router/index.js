import Vue from 'vue'
import router from 'vue-router'
import Book from '../components/Book.vue'
import Patron from '../components/Patron.vue'
import Settings from '../components/Settings.vue'
import Dashboard from '../components/Dashboard.vue'

Vue.use(router)

export default new router ({
  mode: 'history',
  hash: 'false',
    routes: [
          {
            path: '/',
            name: 'Dashboard',
            component: Dashboard
          },
          {
            path: '/book',
            name: 'Book',
            component: Book
          },
          {
            path: '/patron',
            name: 'Patron',
            component: Patron
          },
          {
            path: '/settings',
            name: 'Settings',
            component: Settings
          },
      
    ]
})
