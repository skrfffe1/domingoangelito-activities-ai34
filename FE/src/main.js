import Vue from 'vue'
import App from './App'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCogs, faFileArchive, faHome, faIdCardAlt, faSearch, faSignOutAlt,} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'

library.add(faHome,faIdCardAlt,faFileArchive,faCogs,faSignOutAlt,faSearch)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(BootstrapVue, IconsPlugin)
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
